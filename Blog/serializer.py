from Blog.models import Post
from rest_framework import serializers


class PostSerializer(serializers.ModelSerializer):
    class Meta:
        model= Post
        #fields='__all__'
        fields=('id','title','content','published_at','created_at','updated_at')

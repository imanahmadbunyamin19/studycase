from bdb import Breakpoint
import json
from django.shortcuts import render
from django.http import JsonResponse

from django.views.decorators.csrf import csrf_exempt
from rest_framework.decorators import api_view
from rest_framework.parsers import JSONParser
from rest_framework.response import Response

from Blog.models import Post
from Blog.serializer import PostSerializer

# Create your views here.

@csrf_exempt
def index(request):
    if request.method == 'GET':
        posts = Post.objects.all()
        posts_serializer=PostSerializer(posts, many=True).data
        return JsonResponse(posts_serializer, safe=False)
        
    elif request.method == 'POST':
        post_data = json.loads(request.body)
        post = Post(**post_data) 

        try:
            post.save()
        except:
            return JsonResponse("Failed to Add", safe = False)
        
        post_serializer = PostSerializer(post).data
        return JsonResponse(post_serializer, safe = False)

@csrf_exempt
def detail(request, post_id):
    try:
        post_data=Post.objects.get(id=post_id)
    except Post.DoesNotExist:
        return JsonResponse("Not Found")

    if request.method == 'GET':
        posts_serializer=PostSerializer(post_data)
        return JsonResponse (posts_serializer.data )

    elif request.method == 'PUT':
        post_value=json.loads(request.body)

        post_data.title=post_value['title']
        post_data.content=post_value['content']

        try:
            post_data.save()    
        except:
             return JsonResponse("Not Found")
        post_serializer = PostSerializer(post_data).data
        return JsonResponse(post_serializer, safe = False)

    elif request.method == 'DELETE':
        try:
            post_data.delete()
        except:
            return JsonResponse("Error to Delete")
        post_serializer = PostSerializer(post_data).data
        return JsonResponse(post_serializer, safe = False)
        